FROM ubuntu:18.04

#install dependencies
RUN apt update -y && apt install -y \
  bc \
  curl \
  dosfstools \
  flex \
  gcc \
  gettext \
  git \
  git-lfs \
  gnupg \
  kmod \
  libelf-dev \
  lib32stdc++6 \
  libssl-dev \
  libxml2-utils \
  lzip \
  m4 \
  make \
  mtools \
  openjdk-8-jdk \
  openssh-server \
  python-mako \
  syslinux-utils \
  unzip \
  xorriso \
  zip \
  stgit

# additional dev stuff, not required for build  
RUN apt update -y && apt install -y \
  nano

#install repo tool
RUN curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo
RUN chmod a+x /usr/local/bin/repo

